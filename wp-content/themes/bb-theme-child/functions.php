<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    // wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.min.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    // wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.css");
    //wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
});



// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'gallery-menu' => __( 'Gallery Menu' ),
            'top-bar-menu' => __( 'Top Bar Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Search only products
function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('carpeting','hardwood','laminate','luxury_vinyl_tile','solid_wpc_waterproof','tile'));
    }

    return $query;
}

//add_filter('pre_get_posts','searchfilter');

// Facetwp results
 /*
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );*/
// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

//add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Add to functions.php
function fwp_pagenavi_support() {
?>
<script>
(function($) {
		
    $(document).on('facetwp-refresh', function() {		
        if (! FWP.loaded) {			
			$(document).on('change', '.facetwp-per-page select.facetwp-per-page-select', function(e) { 				
				FWP.extras.per_page =  parseInt($(this).val());
               // FWP.soft_refresh = false;
		        FWP.refresh();
			
			});
            $(document).on('click', '.facetwp-pager a.facetwp-page', function(e) {			
                e.preventDefault();				
                var matches = $(this).attr('data-page').match(/\/page\/(\d+)/);
                if (null != matches) {
                    FWP.paged = parseInt(matches[1]);
					console.log(FWP.paged);
                }
				FWP.paged = parseInt($(this).attr('data-page'));
                //FWP.soft_refresh = false;
		        FWP.refresh();				
            });
        }
    });
})(jQuery);
</script>
<?php
}
//add_action( 'wp_head', 'fwp_pagenavi_support', 50 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


acf_add_options_page( array("page_title"=>"Theme Settings") );



function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
        $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
              if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
              }else{
                return wp_get_attachment_image($id,$size,0,$attr);
              }
        }else if($url){
          return $image_url;
        }
    }
}
/*
function fwp_api_check_permissions() {
    return current_user_can( 'manage_options' );
}
add_filter( 'facetwp_api_can_access', 'fwp_api_check_permissions' );
*/

remove_action( 'wp_head', 'feed_links_extra', 3 );
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
       return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
}
add_shortcode('google_keyword_code', 'new_google_keyword');


// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
			   font-size:2.5em !important;
		   }
      </style>  
   <?php    
}

// Remove query string from static content
function _remove_script_version( $src ){ 
//if(strpos($src,'https://fonts.googleapis') === true) {
	$parts = explode( '?', $src );
	return $parts[0]; 
//}
//return $src;
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 2 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 2 );

function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

function index_is_roomvo_featured( $params, $class ) {
    if ( 'roomvo' == $params['facet_name'] ) { // the facet is named "is_featured"
        $value =  $params['facet_value']; 
        if($value == '0' ){$display_value = 'Not Enabled';}
        if($value == '1' ){$display_value = 'Enabled';}
        if($value == 'Yes' ){$display_value = 'Enabled';}
        if($value == 'No' ){$display_value = 'Not Enabled';}
        $params['facet_display_value'] = $display_value;
    }
    return $params;
}
add_filter( 'facetwp_index_row', 'index_is_roomvo_featured', 10, 2 );

//add method to register event to WordPress init
add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );